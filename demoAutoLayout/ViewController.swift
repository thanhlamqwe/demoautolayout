//
//  ViewController.swift
//  demoAutoLayout
//
//  Created by TL sama on 12/28/19.
//  Copyright © 2019 TL sama. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let dotaImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage.init(named: "Ember"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let descriptionTextView: UITextView = {
        let textView = UITextView()
        let antributeText = NSMutableAttributedString(string: "Ember Spirit - Auto Lose Mid", attributes: [NSAttributedString.Key .font: UIFont.boldSystemFont(ofSize: 18.0)])
        antributeText.append(NSAttributedString(string: "\n\n\nIf You Want to lose your MMR,Just Pick This Shit, get -25mmr and 4 report. High skill Hero play by low skill people. No Thank you!", attributes: [NSAttributedString.Key .font: UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.gray ]))
        textView.attributedText = antributeText
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    private let previousButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("PREV", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.gray, for: .normal)
        return button
    }()
    
    private let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("NEXT", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.gray, for: .normal)
        return button
    }()
    private let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 1
        pc.numberOfPages = 3
        pc.currentPageIndicatorTintColor = .red
        pc.pageIndicatorTintColor = .gray
        return pc
        }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        settupBottomControl()
    }
    private func settupBottomControl() {
        let bottomStackView = UIStackView(arrangedSubviews: [previousButton,pageControl,nextButton])
        bottomStackView.translatesAutoresizingMaskIntoConstraints = false
        bottomStackView.distribution = .fillEqually
        view.addSubview(bottomStackView)
        NSLayoutConstraint.activate([
            bottomStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            bottomStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            bottomStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            bottomStackView.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
    
    private func setupLayout () {
        let topContainerView = UIView()
        view.addSubview(topContainerView)
        topContainerView.translatesAutoresizingMaskIntoConstraints = false
        topContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topContainerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topContainerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        topContainerView.addSubview(dotaImageView)
        dotaImageView.centerXAnchor.constraint(equalTo: topContainerView.centerXAnchor).isActive = true
        dotaImageView.centerYAnchor.constraint(equalTo: topContainerView.centerYAnchor).isActive = true
        dotaImageView.heightAnchor.constraint(equalTo: topContainerView.heightAnchor, multiplier: 0.8).isActive = true
        view.addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: topContainerView.bottomAnchor).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 25 ).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: view.rightAnchor, constant:  -25).isActive = true

    }
}

